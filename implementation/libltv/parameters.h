#ifndef PARAMETERS_H_GEUM0TS8
#define PARAMETERS_H_GEUM0TS8
/******************************************************************************
* File:             parameters.h
*
* Author:           Alex Dalton  
* Created:          12/04/19 
* Description:      Handles LTV parameter defintions
*****************************************************************************/

#include <stdint.h>
#include <gmp.h>

typedef struct P {
    uint64_t n;             // ring dimension
    uint64_t p;             // plaintext modulus
    gmp_randstate_t state;  // pseudorandom generator state
    mpz_t* q_factors;       // factors of ciphertext modulus
    mpz_t* inv_q_factors;   // inverse of factors of ciphertext modulus mod p
    mpz_t q;                // ciphertext modulus
    int depth;              // the acceptable computation depth
} Parameters;

/**
 * generateParameters mallocs and generates the full LTV parameters given user defined
 * parameters
 */
Parameters* generateParameters(uint64_t n, uint64_t p, int depth);

void freeParameters(Parameters* p);

#endif /* end of include guard: PARAMETERS_H_GEUM0TS8 */
