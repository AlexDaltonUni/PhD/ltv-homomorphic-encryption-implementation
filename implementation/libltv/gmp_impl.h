#ifndef GMP_IMPL_H_LRIR1Q2I
#define GMP_IMPL_H_LRIR1Q2I
/******************************************************************************
* File:             gmp_impl.h
*
* Author:           Alex Dalton
* Created:          12/04/19 
* Description:      LTV encryption implimentation using GNU Multiple Precission
*                   library. Ref: https://github.com/tbuktu/libntru
*****************************************************************************/
#include <stdio.h>
#include <gmp.h>
#include "parameters.h"
#include "gmp_ring_arithmetic.h"

typedef Polynomial PlainText;
typedef struct {
    Polynomial poly;
    mpz_t mod;
    int degree;
} CipherText;

typedef Polynomial SecretKey;
typedef Polynomial PublicKey;
typedef struct KeyPair {
    SecretKey sk;
    PublicKey pk;
} KeyPair;

/**
 * newPlainText  initialises a PlainText structure, initialising the Polynomial
 * newCipherText initialises a CipherText structure, initialising the Polynomial mod q
 */
PlainText* newPlainText(const int order);
CipherText* newCipherText(const int order, const mpz_t q);

void coefReduction(Polynomial out, const Polynomial in, const mpz_t mod);

/**
 * freePlainText dealocates the memory heald PlainText structure
 * freeCipherText dealocates the memory heald CipherText structure
 */
void freePlainText(PlainText* p);
void freeCipherText(CipherText* c);

/**
 * KeyGen generates a (SecretKey,PublicKey) pair of keys given Parameters p
 */
KeyPair* KeyGen(Parameters* p);

/**
 * Enc encrypts a plaintext m under public key pk, given Parameters p, and stores
 * this new value in c, which is assumed to be initialised
 */
void Enc(CipherText* c, const PlainText* m, const PublicKey pk, Parameters* p);

/**
 * Dec decrypts a ciphertext c under secret key sk given Parameters p, and stores
 * this new value n m, which s assumed to be initialised
 */
void Dec(PlainText* m, const CipherText* c, const SecretKey sk, const Parameters* p);

/**
 * EvalAdd performs an homomorphic addition s.t Dec(out) = Dec(a) + Dec(b)
 * EvalSub performs an homomorphic subtraction s.t Dec(out) = Dec(a) - Dec(b)
 */
void EvalAdd(CipherText* out, CipherText* a, CipherText* b, const Parameters* p);
void EvalSub(CipherText* out, CipherText* a, CipherText* b, const Parameters* p);
void EvalMul(CipherText* out, CipherText* a, CipherText* b, const Parameters* p);

/**
 * KeySwitch creates a ciphertext of degree 1 encrypted under f_2 given a ciphertext
 * of degree d encrypted under f_1 provided hint = m * f_1 ^ d * f_2 ^ -1 mod p->q
 */
void KeySwitchHint(Polynomial hint, int degree, const SecretKey f_1, const SecretKey f_2, Parameters* p);
void KeySwitch(CipherText* out, const CipherText* in, const Polynomial hint, const Parameters* p);

/**
 * ModReduction translates a ciphertext in ring R_(in->mod) to one in R_(in->mod/q'),
 * v is a precomputed inverse of q' mod p->p
 */
void ModReduction(CipherText* out, const CipherText* in, const mpz_t q_prime, const mpz_t v, const Parameters* p);

/**
 * modMatch ensures that both ciphertexts are encrypted under the same modulus
 */
void modMatch(CipherText* a, CipherText *b, const Parameters* p);

/**
 * RingReduction translates a ciphertext in a ring of dimension n to one of dimension n/2
 */
void RingReduction(CipherText* out, const CipherText* in, const Polynomial sparse_hint, Parameters *p);

/**
 * CompEvalMul performs EvalMult, KeySwitch, and ModReduction and sometimes TODO: RingReduction
 */
void CompEvalMul(CipherText* out, CipherText* a, CipherText* b, const Polynomial keyswitch_hint, const Parameters *p);

#endif /* end of include guard: GMP_IMPL_H_LRIR1Q2I */
