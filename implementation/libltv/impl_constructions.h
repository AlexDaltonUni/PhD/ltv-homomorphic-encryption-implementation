#ifndef IMPL_CONSTRUCTIONS_H_LRIR1Q2I
#define IMPL_CONSTRUCTIONS_H_LRIR1Q2I
/******************************************************************************
* File:             impl_constructions.h
*
* Author:           Alex Dalton
* Created:          02/03/20
* Description:      Homomorphic constuctions to achieve ciphertext comparisions etc.
*****************************************************************************/

#include "crt.h"

typedef struct {
    DblCRTCipherText* values;
    int max_index;
} CipherTextSet;        // a struct to hold a set of ciphertexts, each representing a single binary value
                        // of the plaintext

void initCipherTextSet(CipherTextSet** c, int bits, Parameters* p);
void freeCipherTextSet(CipherTextSet* in);

/**
 * DecomposeEnc decomposes and encrypyts a 64 bit unsigned integer into 64 LTV ciphertexts,
 * each representing a single bit, it allocates the space for the ciphertexts
 */
CipherTextSet* decomposeEnc(const uint64_t in, const PublicKey pk, Parameters* p, int bits);

/**
 * ComposeDec decrypts and composes 64 ciphertexts into a single unsigned 64 bit integer,
 * it frees the space for the CipherTexts
 */
uint64_t composeDec(CipherTextSet *in, const SecretKey sk, Parameters* p);

/**
 * Comparison functions which return 1 if a > b (comparing plaintexts) and returns a pointer to
 * the encrypted result
 */
DblCRTCipherText compareGreaterThanInt(const CipherTextSet *a, const uint64_t b, const PublicKey pk, const DblCRT hint, Parameters* p);
// TODO: CipherText* compareGreaterThanEnc(const CipherTextSet *a, const CipherTextSet* b, const PublicKey pk, const Polynomial hint, Parameters* p);

/**
 * halfAdder sets s = a XOR b, c = a AND b, given a hint from the joint key of a and b to a desired result key
 * fullAdder sets s = a XOR b XOR c_in, c_out = (a AND b) OR ((a XOR b) AND c_in), required a minimum computational depth of 3
 * streamAdder sets out = a + b, requires a minimum computational depth of 3 * out->max_index
 * streamAdderBit sets out = a + b, where b is a single binary value and requires a minimum computational depth of 3 * out->max_index
 */
void halfAdder(DblCRTCipherText* s, DblCRTCipherText* c, DblCRTCipherText a, DblCRTCipherText b, const DblCRT hint, Parameters* p);
void fullAdder(DblCRTCipherText* s, DblCRTCipherText* c_out, DblCRTCipherText a, DblCRTCipherText b, const DblCRTCipherText c_in, const DblCRT hint, Parameters* p);
void streamAdder(CipherTextSet* out, const CipherTextSet a, const CipherTextSet b, const DblCRT hint, Parameters* p);
void streamAdderBit(CipherTextSet* out, const CipherTextSet a, const DblCRTCipherText b, const DblCRT hint, const PublicKey pk, Parameters* p);

#endif /* end of include guard: IMPL_CONSTRUCTIONS_H_LRIR1Q2I */
