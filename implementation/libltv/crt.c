#include <stdlib.h>
#include "crt.h"
#include <string.h>

DblCRT newDblCRT(int t, int n)
{
    DblCRT ret;
    ret.t = t;
    ret.n = n;
    ret.values = malloc(sizeof(uint64_t*) * ret.t);
    for(int i = 0; i < ret.t; i++)
    {
        ret.values[i] = malloc(sizeof(uint64_t) * ret.n);
    }
    return ret;
}

DblCRT CRT(Polynomial in, const Parameters* p)
{
    DblCRT ret = newDblCRT(p->depth, p->n);

#pragma omp parallel for collapse(2)
    for(int i = 0; i < ret.t; i++)
    {
        for(int j = 0; j < ret.n; j++)
        {
            mpz_t scratch;
            mpz_init(scratch);
            mpz_mod(scratch, in.co[j], p->q_factors[i]);
            ret.values[i][j] = mpz_get_ui(scratch);
            mpz_clear(scratch);
        }
    }

    return ret;
}

void freeDblCRT(DblCRT in)
{
    for(int i = 0; i < in.t; i++) free(in.values[i]);
    free(in.values);
}

void iCRT(Polynomial out, DblCRT in, const mpz_t mod, const int depth, const Parameters* p)
{
    setPoly(out, 0);
    Polynomial temp = { NULL, p->n };
    initPoly(&temp);
    mpz_t scratch;
    mpz_t mod_div;
    mpz_init(mod_div);
    mpz_init(scratch);

    for(int i = depth; i < in.t; i++)
    {
        mpz_mod(scratch, mod, p->q_factors[i]);
        if(mpz_cmp_ui(scratch, 0) == 0)
        {
            mpz_div(mod_div, mod, p->q_factors[i]);
            mpz_invert(scratch, mod_div, p->q_factors[i]);
            for(int j = 0; j < in.n; j++)
            {
                mpz_mul_ui(temp.co[j], scratch, in.values[i][j]);
                mpz_mod(temp.co[j], temp.co[j], p->q_factors[i]);
                mpz_mul(temp.co[j], temp.co[j], mod_div);
                mpz_add(out.co[j], out.co[j], temp.co[j]);
                mpz_mod(out.co[j], out.co[j], mod);
            }
        }
    }

    mpz_clear(scratch);
    mpz_clear(mod_div);
    freePoly(&temp);
}

void printDblCRT(DblCRT in)
{
    for(int i = 0; i < in.t; i++)
    {
        printf("[");
        for(int j = 0; j < in.n; j++)
        {
            printf(" %lu", in.values[i][j]);
        }
        printf(" ]\n");
    }
}

DblCRTCipherText newDblCRTCipherText(const Parameters* p)
{
    DblCRTCipherText ret;
    mpz_init(ret.mod);
    ret.poly = newDblCRT(p->depth, p->n);
    ret.degree = 0;
    ret.current_depth = 0;

    return ret;
}

void freeDblCRTCipherText(DblCRTCipherText in)
{
    freeDblCRT(in.poly);
    mpz_clear(in.mod);
}

DblCRTCipherText CRTEnc(const PlainText* m, const PublicKey pk, Parameters* p)
{
    DblCRTCipherText c;
    CipherText* temp = newCipherText(p->n, p->q);
    Enc(temp, m, pk, p);
    c.poly = CRT(temp->poly, p);
    mpz_init(c.mod);
    mpz_set(c.mod, temp->mod);
    c.degree = 1;
    c.current_depth = 0;
    freeCipherText(temp);

    return c;
}

void CRTDec(PlainText* m, DblCRTCipherText c, const SecretKey sk, const Parameters* p)
{
    CipherText* temp = newCipherText(p->n, p->q);
    iCRT(temp->poly, c.poly, c.mod, c.current_depth, p);
    mpz_set(temp->mod, c.mod);
    temp->degree = c.degree;
    Dec(m, temp, sk, p);
    freeCipherText(temp);
    mpz_clear(c.mod);
}

void CRTRingAdd(DblCRT out, const DblCRT a, const DblCRT b, const int current_depth, const Parameters* p)
{
#pragma omp parallel for collapse(2)
    for(int i = current_depth; i < out.t; i++)
    {
        for(int j = 0; j < out.n; j++)
        {
            uint64_t mod = mpz_get_ui(p->q_factors[i]);
            out.values[i][j] = (a.values[i][j] + b.values[i][j]) % mod;
        }
    }
}

void CRTRingSub(DblCRT out, const DblCRT a, const DblCRT b, const int current_depth, const Parameters* p)
{
#pragma omp parallel for collapse(2)
    for(int i = current_depth; i < out.t; i++)
    {
        for(int j = 0; j < out.n; j++)
        {
            uint64_t mod = mpz_get_ui(p->q_factors[i]);
            long long val = a.values[i][j] + b.values[i][j];
            out.values[i][j] = val % mod;
        }
    }
}

void CRTEvalAdd(DblCRTCipherText* out, DblCRTCipherText* a, DblCRTCipherText* b, const Parameters* p)
{
    CRTModMatch(a, b, p);
    CRTRingAdd(out->poly, a->poly, b->poly, a->current_depth, p);
    out->degree = a->degree;
    out->current_depth = a->current_depth;
    mpz_set(out->mod, a->mod);
}

void CRTEvalSub(DblCRTCipherText* out, DblCRTCipherText* a, DblCRTCipherText* b, const Parameters* p)
{
    CRTModMatch(a, b, p);
    CRTRingSub(out->poly, a->poly, b->poly, a->current_depth, p);
    out->degree = a->degree;
    out->current_depth = a->current_depth;
    mpz_set(out->mod, a->mod);
}

uint64_t modMul(uint64_t a, uint64_t b, uint64_t mod)
{
    uint64_t res = 0;
    a = a % mod; 
    while (b > 0) 
    { 
        if (b % 2 == 1) res = (res + a) % mod; 
        a = (a * 2) % mod; 
        b /= 2; 
    } 

    return res % mod; 
}

void CRTRingMul(DblCRT out, const DblCRT a, const DblCRT b, const mpz_t mod, const int current_depth, const Parameters* p)
{
#pragma omp parallel for
    for(int i = current_depth; i < out.t; i++)
    {
        uint64_t t_mod = mpz_get_ui(p->q_factors[i]);
        uint64_t temp[out.n];
        for(int j = 0; j < out.n; j++) temp[j] = 0;

        for(int x = 0; x < a.n; x++)
        {
            for(int y = 0; y < b.n; y++)
            {
                uint64_t val = modMul(a.values[i][x], b.values[i][y], t_mod);
                if(x + y >= out.n) val = t_mod - val;
                temp[(x + y) % out.n] = (temp[(x + y) % out.n] + val) % t_mod;
            }
        }

        for(int j = 0; j < out.n; j++) out.values[i][j] = temp[j];
    }
}

void CRTEvalMul(DblCRTCipherText* out, DblCRTCipherText* a, DblCRTCipherText* b, const Parameters* p)
{
    CRTModMatch(a, b, p);
    CRTRingMul(out->poly, a->poly, b->poly, a->mod, a->current_depth, p);
    out->degree = a->degree + b->degree;
    out->current_depth = a->current_depth;
    mpz_set(out->mod, a->mod);
}

DblCRT CRTKeySwitchHint(int degree, const SecretKey f_1, const SecretKey f_2, Parameters* p)
{
    Polynomial poly = { NULL, p->n };
    initPoly(&poly);

    KeySwitchHint(poly, degree, f_1, f_2, p);
    DblCRT hint = CRT(poly, p);

    freePoly(&poly);
    return hint;
}

void CRTKeySwitch(DblCRTCipherText* out, const DblCRTCipherText in, const DblCRT hint, const Parameters* p)
{
    CRTRingMul(out->poly, in.poly, hint, in.mod, in.current_depth, p);
    out->degree = 1;
    out->current_depth = in.current_depth;
}

void CRTModReduction(DblCRTCipherText *out, const DblCRTCipherText in, const mpz_t q_prime, const mpz_t inv_q_prime, const Parameters* p)
{
    Polynomial d = { NULL, p->n };
    initPoly(&d);
    mpz_t scratch;
    mpz_init(scratch);
    mpz_set(scratch, q_prime);

    // computing d = c mod q'
#pragma omp parallel for
    for(int i = 0; i < p->n; i++)
    {
        mpz_set_ui(d.co[i], in.poly.values[in.current_depth][i]);
    }
    coefReduction(d, d, q_prime);

    // compute delta = (vq' - 1) * d mod (pq')
    mpz_t p_q_prime;
    mpz_init(p_q_prime);
    mpz_mul_ui(p_q_prime, q_prime, p->p);
    mpz_mul(scratch, scratch, inv_q_prime);
    mpz_sub_ui(scratch, scratch, 1);
#pragma omp parallel for
    for(int i = 0; i < p->n; i++)
    {
        mpz_mul(d.co[i], d.co[i], scratch);
        mpz_mod(d.co[i], d.co[i], p_q_prime);
    }

    // add delta to c
    DblCRT delta = CRT(d, p);
    for(int i = in.current_depth; i < p->depth; i++)
    {
        mpz_t local_scratch;
        mpz_init(local_scratch);
        mpz_mod(local_scratch, in.mod, p->q_factors[i]);
#pragma omp parallel for
        for(int j = 0; j < p->n; j++)
        {
            out->poly.values[i][j] = (in.poly.values[i][j] + delta.values[i][j]) % mpz_get_ui(p->q_factors[i]);
        }
        mpz_clear(local_scratch);
    }

    // compute d'/q'
    for(int i = in.current_depth + 1; i < p->depth; i++)
    {
        mpz_t local_scratch2;
        mpz_init(local_scratch2);
        modInv(local_scratch2, q_prime, p->q_factors[i]);
#pragma omp parallel for
        for(int j = 0; j < p->n; j++)
        {
            out->poly.values[i][j] = modMul(out->poly.values[i][j], mpz_get_ui(local_scratch2), mpz_get_ui(p->q_factors[i]));
        }
        mpz_clear(local_scratch2);
    }

    mpz_fdiv_q(out->mod, in.mod, q_prime);
    out->current_depth = in.current_depth + 1;

    freePoly(&d);
    mpz_clear(scratch);
}

void CRTModMatch(DblCRTCipherText* a, DblCRTCipherText* b, const Parameters* p)
{
    while(a->current_depth < b->current_depth) CRTModReduction(a, *a, p->q_factors[a->current_depth], p->inv_q_factors[a->current_depth], p);
    while(b->current_depth < a->current_depth) CRTModReduction(b, *b, p->q_factors[b->current_depth], p->inv_q_factors[b->current_depth], p);
}

void CRTCompEvalMul(DblCRTCipherText* out, DblCRTCipherText* a, DblCRTCipherText* b, const DblCRT hint, const Parameters* p)
{
    CRTEvalMul(out, a, b, p);
    CRTKeySwitch(out, *out, hint, p);
    CRTModReduction(out, *out, p->q_factors[out->current_depth], p->inv_q_factors[out->current_depth], p);
}

void CRTCopyPoly(DblCRT out, DblCRT in)
{
#pragma omp parallel for collapse(2)
    for(int i = 0; i < out.t; i++)
    {
        for(int j = 0; j < out.n; j++)
        {
            out.values[i][j] = in.values[i][j];
        }
    }
}
