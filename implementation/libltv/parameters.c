#include "parameters.h"
#include "gmp_impl.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

Parameters* generateParameters(uint64_t n, uint64_t p, int depth)
{
    printf("Generating parameters...");
    Parameters* param = (Parameters*) malloc(sizeof(Parameters));

    param->n = n;
    param->p = p;
    param->depth = depth;

    // TODO: Not cryptographically secure randomness, only used for testing
    srand(time(0));
    uint64_t seed = rand();
    gmp_randinit_mt(param->state);
    gmp_randseed_ui(param->state, seed);

    param->q_factors = (mpz_t*)malloc(sizeof(mpz_t) * depth);
    param->inv_q_factors = (mpz_t*)malloc(sizeof(mpz_t) * depth);
    mpz_init(param->q);
    mpz_set_ui(param->q, 1);

    mpz_t mp_p;
    mpz_init(mp_p);
    mpz_set_ui(mp_p, 2);

    int prime = 0;
    for(int i = 0; i < depth; i++)
    {
        mpz_init(param->q_factors[i]);
        mpz_init(param->inv_q_factors[i]);
        while(!prime)
        {
            mpz_urandomb (param->q_factors[i], param->state, 60);
            if (mpz_probab_prime_p(param->q_factors[i], 50)) prime = 1;
        }
        mpz_mul(param->q, param->q, param->q_factors[i]);
        modInv(param->inv_q_factors[i], param->q_factors[i], mp_p);

        prime = 0;
    }

    mpz_clear(mp_p);
    
    printf("done.\n");
    return param;
}

void freeParameters(Parameters* p)
{
    mpz_clear(p->q);
    for(int i = 0; i < p->depth; i++)
    {
        mpz_clear(p->q_factors[i]);
        mpz_clear(p->inv_q_factors[i]);
    }
    free((mpz_t*) p->q_factors);
    free((mpz_t*) p->inv_q_factors);
    free((Parameters*) p);
}
