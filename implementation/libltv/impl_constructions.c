#include "impl_constructions.h"
#include <stdlib.h>

void initCipherTextSet(CipherTextSet** c, int bits, Parameters* p)
{
    (*c) = malloc(sizeof(CipherTextSet));
    (*c)->values = malloc(sizeof(DblCRTCipherText) * bits);
    (*c)->max_index = bits;
}

CipherTextSet* decomposeEnc(const uint64_t in, const PublicKey pk, Parameters* p, int bits)
{
    CipherTextSet* c;
    initCipherTextSet(&c, bits, p);

#pragma omp parallel for
    for(int i = 0; i < c->max_index; i++)
    {
        PlainText* m = newPlainText(p->n);
        setPoly(*m, 0);
        uint64_t bit = (in >> i) & 1;
        mpz_set_ui(m->co[0], bit);

        c->values[i] = CRTEnc(m, pk, p);
        freePlainText(m);
    }

    return c;
}

void freeCipherTextSet(CipherTextSet *in)
{
    free(in->values);
    free(in);
}

uint64_t composeDec(CipherTextSet* in, const SecretKey sk, Parameters* p)
{
    uint64_t output = 0;
#pragma omp parallel for reduction(+:output)
    for(int i = 0; i < in->max_index; i++)
    {
        PlainText* m = newPlainText(p->n);
        CRTDec(m, in->values[i], sk, p);
        uint64_t bit = mpz_get_ui(m->co[0]);
        freePlainText(m);
        output += bit << i;
    }

    freeCipherTextSet(in);

    return output;
}

DblCRTCipherText compareGreaterThanInt(const CipherTextSet *a, const uint64_t b, const PublicKey pk, const DblCRT hint, Parameters* p)
{
    PlainText* z_m = newPlainText(p->n);
    PlainText* one_m = newPlainText(p->n);
    PlainText* check = newPlainText(p->n);
    setPoly(*z_m, 0);
    setPoly(*one_m, 0);
    mpz_set_ui(one_m->co[0], 1);
    DblCRTCipherText z = CRTEnc(z_m, pk, p);
    DblCRTCipherText one = CRTEnc(one_m, pk, p);
    DblCRTCipherText scratch = newDblCRTCipherText(p);

    uint64_t max_a = 1 << (a->max_index - 1);

    if(b <= max_a)
    {
        for(int i = 0; i < a->max_index; i++)
        {
            uint64_t bit = (b >> i) & 1;
            if(bit == 1)
            {
                CRTCompEvalMul(&z, &(a->values[i]), &z, hint, p);
            }
            else
            {
                CRTEvalSub(&scratch, &one, &(a->values[i]), p);
                CRTCompEvalMul(&scratch, &scratch, &z, hint, p);
                CRTEvalAdd(&z, &(a->values[i]), &scratch, p);
            }
        }
    }

    freeDblCRTCipherText(one);
    freeDblCRTCipherText(scratch);
    return z;
}

void halfAdder(DblCRTCipherText* s, DblCRTCipherText* c, DblCRTCipherText a, DblCRTCipherText b, const DblCRT hint, Parameters* p)
{
    CRTEvalAdd(s, &a, &b, p);
    CRTCompEvalMul(c, &a, &b, hint, p);
}

void fullAdder(DblCRTCipherText* s, DblCRTCipherText* c_out, DblCRTCipherText a, DblCRTCipherText b, const DblCRTCipherText c_in, const DblCRT hint, Parameters* p)
{
    DblCRTCipherText a_XOR_b = newDblCRTCipherText(p);
    DblCRTCipherText a_AND_b = newDblCRTCipherText(p);
    DblCRTCipherText c_out_temp = newDblCRTCipherText(p);
    DblCRTCipherText scratch1 = newDblCRTCipherText(p);
    DblCRTCipherText scratch2 = newDblCRTCipherText(p);

    halfAdder(&a_XOR_b, &a_AND_b, a, b, hint, p);
    halfAdder(s, &c_out_temp, a_XOR_b, c_in, hint, p);

    CRTEvalAdd(&scratch1, &c_out_temp, &a_AND_b, p);
    CRTCompEvalMul(&scratch2, &c_out_temp, &a_AND_b, hint, p);
    CRTEvalAdd(c_out, &scratch1, &scratch2, p);

    freeDblCRTCipherText(a_XOR_b);
    freeDblCRTCipherText(a_AND_b);
    freeDblCRTCipherText(c_out_temp);
    freeDblCRTCipherText(scratch1);
    freeDblCRTCipherText(scratch2);
}

void streamAdder(CipherTextSet* out, const CipherTextSet a, const CipherTextSet b, const DblCRT hint, Parameters* p)
{
    DblCRTCipherText c = newDblCRTCipherText(p);

    CipherTextSet* temp;
    initCipherTextSet(&temp, out->max_index, p);
#pragma omp parallel for
    for(int i = 0; i < temp->max_index; i++) temp->values[i] = newDblCRTCipherText(p);

    for(int i = 0; i < out->max_index; i++)
    {
        if(i == 0) halfAdder(&(temp->values[i]), &c, a.values[i], b.values[i], hint, p);
        else fullAdder(&(temp->values[i]), &c, a.values[i], b.values[i], c, hint, p);
    }

#pragma omp parallel for
    for(int i = 0; i < out->max_index; i++)
    {
        CRTCopyPoly(out->values[i].poly, temp->values[i].poly);
        mpz_set(out->values[i].mod, temp->values[i].mod);
        out->values[i].degree = temp->values[i].degree;
        out->values[i].current_depth = temp->values[i].current_depth;
    }

    freeCipherTextSet(temp);
    freeDblCRTCipherText(c);
}

void streamAdderBit(CipherTextSet* out, const CipherTextSet a, const DblCRTCipherText b, const DblCRT hint, const PublicKey pk, Parameters* p)
{
    CipherTextSet* b_expanded = decomposeEnc(0, pk, p, out->max_index);
    DblCRTCipherText b_replaced = b_expanded->values[0];
    b_expanded->values[0] = b;

    streamAdder(out, a, *b_expanded, hint, p);

    b_expanded->values[0] = b_replaced;
    freeCipherTextSet(b_expanded);
}
