#ifndef CRT_H_906VSSHA
#define CRT_H_906VSSHA
/******************************************************************************
* File:             crt.h
*
* Author:           Alex Dalton  
* Created:          12/04/19 
* Description:      Functions for handing double Chinese Remainder Transform
*                   representations of variables
*****************************************************************************/

#include <gmp.h>
#include "parameters.h"
#include "gmp_impl.h"

/************************************************************************
 * Structs
 ***********************************************************************
 * DblCRT representation of a polynomial of order n and modulus q consists of
 * t length-n vectors of mod q_t values
 *
 * DblCRTCipherText represents a ciphertext with a DblCRT polynomial
 *
 * DblCRTCipherTextSet holds a set of DblCRTCipherTexts
 ***********************************************************************/
typedef struct {
    uint64_t** values;
    int t;
    int n;
} DblCRT;

typedef struct {
    DblCRT poly;
    mpz_t mod;
    int degree;
    int current_depth;
} DblCRTCipherText;

/***********************************************************************
 * CRT Transform
 ***********************************************************************
 * CRT transforms a polynomial into DblCRT representation and instatiates all the
 * required memory
 *
 * iCRT performs the inverse operation and frees the memory
 ***********************************************************************/
DblCRT newDblCRT(int t, int n);
void freeDblCRT(DblCRT in);
DblCRT CRT(Polynomial in, const Parameters* p);
void iCRT(Polynomial out, DblCRT in, const mpz_t mod, const int depth, const Parameters* p);
void printDblCRT(DblCRT in);

DblCRTCipherText newDblCRTCipherText(const Parameters* p);
void freeDblCRTCipherText(DblCRTCipherText in);

/************************************************************************
 * LTV Encryption and Decryption
 ***********************************************************************/
DblCRTCipherText CRTEnc(const PlainText* m, const PublicKey pk, Parameters* p);
void CRTDec(PlainText* m, DblCRTCipherText c, const SecretKey sk, const Parameters* p);

/************************************************************************
 * Homomorphic Arithmetic Functions
 ***********************************************************************
 * CRTEvalAdd adds two ciphertexts together
 * CRTEvalMul multiplies two ciphertexts together
 * CRTModReduction performs modulus reduction on a ciphertext
 * CRTModMatch reduces two ciphertexts to the same modulus
 * CRTKeySwitchHint generates the a hint required for keyswitching in CRT form
 * CRTCompEvalMul performs the composed EvalMul operation
 ***********************************************************************/
void CRTEvalAdd(DblCRTCipherText* out, DblCRTCipherText* a, DblCRTCipherText* b, const Parameters* p);
void CRTEvalSub(DblCRTCipherText* out, DblCRTCipherText* a, DblCRTCipherText* b, const Parameters* p);
void CRTRingMul(DblCRT out, const DblCRT a, const DblCRT b, const mpz_t mod, const int current_depth, const Parameters* p);
void CRTEvalMul(DblCRTCipherText* out, DblCRTCipherText* a, DblCRTCipherText* b, const Parameters* p);
DblCRT CRTKeySwitchHint(int degree, const SecretKey f_1, const SecretKey f_2, Parameters* p);
void CRTKeySwitch(DblCRTCipherText* out, const DblCRTCipherText in, const DblCRT hint, const Parameters* p);
void CRTModReduction(DblCRTCipherText* out, const DblCRTCipherText in, const mpz_t q_prime, const mpz_t inv_q_prime, const Parameters* p);
void CRTModMatch(DblCRTCipherText* a, DblCRTCipherText* b, const Parameters* p);
void CRTCompEvalMul(DblCRTCipherText* out, DblCRTCipherText* a, DblCRTCipherText* b, const DblCRT hint, const Parameters* p);

void CRTCopyPoly(DblCRT out, DblCRT in);

#endif /* end of include guard: CRT_H_906VSSHA */
