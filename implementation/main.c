#include <stdio.h>
#include <stdlib.h>
#include "libltv/gmp_impl.h"

int main() {
    Parameters* p = generateParameters(64, 2, 100);
    KeyPair* a_keys = KeyGen(p);
    KeyPair* b_keys = KeyGen(p);
    SecretKey ab = { NULL, p->n };
    initPoly(&ab);
    ringMul(ab, a_keys->sk, b_keys->sk, p);

    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    CipherText* a_c = newCipherText(p->n, p->q);
    CipherText* b_c = newCipherText(p->n, p->q);
    CipherText* add = newCipherText(p->n, p->q);
    PlainText* out = newPlainText(p->n);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a_m->co[i], p->state, m_mod);
        mpz_urandomm(b_m->co[i], p->state, m_mod);
    }

    Enc(a_c, a_m, a_keys->pk, p);
    Enc(b_c, b_m, b_keys->pk, p);
    EvalAdd(add, a_c, b_c, p);
    Dec(out, add, ab, p);

    printf("a     : "); printPoly(*a_m);
    printf("b     : "); printPoly(*b_m);
    printf("a + b : "); printPoly(*out);

    freePlainText(a_m);
    freePlainText(b_m);
    freePlainText(out);
    freeCipherText(a_c);
    freeCipherText(b_c);
    freeCipherText(add);
    freeParameters(p);
    mpz_clear(m_mod);
}
