#!/bin/bash

mkdir -p build
cd build
cmake ..
make
CPUPROFILE=/tmp/prof.out OMP_NUM_THREADS=1 ./bloodpressure 64
pprof bloodpressure /tmp/prof.out >> libltv_profile
