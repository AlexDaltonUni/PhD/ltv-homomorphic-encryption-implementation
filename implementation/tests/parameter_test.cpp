#include <gtest/gtest.h>
#include <gmp.h>

extern "C" {
#include "../libltv/parameters.h"
}

TEST(ParameterTest, ParameterGeneration)
{
    const Parameters* p = generateParameters(1024, 2, 5);

    // check p
    ASSERT_NE(p, nullptr);

    // check p->n
    ASSERT_EQ(p->n, 1024);

    // check p->p
    ASSERT_EQ(p->p, 2);

    // check p->q is coprime with p->p
    mpz_t div;
    mpz_init(div);
    mpz_gcd_ui(div, p->q, p->p);
    ASSERT_EQ(mpz_cmp_ui(div, 1), 0);

    // check all q_factors multiply to make q
    mpz_t q_check;
    mpz_init(q_check);
    mpz_set_ui(q_check, 1);
    for(int i = 0; i < p->depth; i++) mpz_mul(q_check, q_check, p->q_factors[i]);
    ASSERT_EQ(mpz_cmp(p->q, q_check), 0);

    for(int i = 0; i < p->depth; i++)
    {
        mpz_mul(q_check, p->q_factors[i], p->inv_q_factors[i]);
        mpz_mod_ui(q_check, q_check, p->p);
        ASSERT_EQ(mpz_cmp_ui(q_check, 1), 0);
    }
    
    mpz_clear(q_check);
    free((Parameters *)p);
}
