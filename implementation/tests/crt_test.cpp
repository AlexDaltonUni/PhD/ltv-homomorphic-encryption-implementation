#include <gtest/gtest.h>
#include <gmp.h>

extern "C" {
#include "../libltv/crt.h"
}

TEST(CRTTest, CRTInvertable)
{
    Parameters* p = generateParameters(256, 2, 1);
    Polynomial a = { NULL, p->n };
    Polynomial a_prime = { NULL, p->n };
    initPoly(&a);
    initPoly(&a_prime);

    for(uint64_t i = 0; i < a.order; i++)
    {
        mpz_urandomm(a.co[i], p->state, p->q);
    }

    DblCRT a_crt = CRT(a, p);
    iCRT(a_prime, a_crt, p->q, 0, p);

    for(uint64_t i = 0; i < a.order; i++)
    {
        ASSERT_EQ(mpz_cmp(a.co[i], a_prime.co[i]), 0);
    }

    freePoly(&a);
    freePoly(&a_prime);
}

TEST(CRTTest, Completeness)
{
    Parameters* p = generateParameters(256, 2, 10);
    KeyPair* keys = KeyGen(p);
    PlainText* a = newPlainText(p->n);
    PlainText* a_prime = newPlainText(p->n);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < a->order; i++)
    {
        mpz_urandomm(a->co[i], p->state, m_mod);
    }
    mpz_clear(m_mod);

    DblCRTCipherText c = CRTEnc(a, keys->pk, p);
    CRTDec(a_prime, c, keys->sk, p);

    for(uint64_t i = 0; i < a->order; i++)
    {
        ASSERT_EQ(mpz_cmp(a->co[i], a_prime->co[i]), 0);
    }

    freePlainText(a);
    freePlainText(a_prime);
}

TEST(CRTTest, EvalAdd)
{
    Parameters* p = generateParameters(64, 2, 2);
    KeyPair* keys = KeyGen(p);
    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    PlainText* output_m = newPlainText(p->n);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < a_m->order; i++)
    {
        mpz_urandomm(a_m->co[i], p->state, m_mod);
        mpz_urandomm(b_m->co[i], p->state, m_mod);
    }
    mpz_clear(m_mod);

    DblCRTCipherText a_c = CRTEnc(a_m, keys->pk, p);
    DblCRTCipherText b_c = CRTEnc(b_m, keys->pk, p);

    CRTEvalAdd(&a_c, &a_c, &b_c, p);

    CRTDec(output_m, a_c, keys->sk, p);

    for(uint64_t i = 0; i < a_m->order; i++)
    {
        char a_bit = mpz_get_ui(a_m->co[i]);
        char b_bit = mpz_get_ui(b_m->co[i]);
        ASSERT_EQ(mpz_cmp_ui(output_m->co[i], a_bit ^ b_bit), 0);
    }

    freePlainText(a_m);
    freePlainText(b_m);
    freePlainText(output_m);
}

TEST(CRTTest, EvalMul)
{
    Parameters* p = generateParameters(64, 2, 2);
    KeyPair* keys = KeyGen(p);
    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    PlainText* output_m = newPlainText(p->n);
    PlainText* check = newPlainText(p->n);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < a_m->order; i++)
    {
        mpz_urandomm(a_m->co[i], p->state, m_mod);
        mpz_urandomm(b_m->co[i], p->state, m_mod);
    }

    ringMulModOrder(*check, *a_m, *b_m, m_mod, p->n);

    DblCRTCipherText a_c = CRTEnc(a_m, keys->pk, p);
    DblCRTCipherText b_c = CRTEnc(b_m, keys->pk, p);

    CRTEvalMul(&a_c, &a_c, &b_c, p);

    ringMul(keys->sk, keys->sk, keys->sk, p);
    CRTDec(output_m, a_c, keys->sk, p);

    for(uint64_t i = 0; i < a_m->order; i++)
    {
        ASSERT_EQ(mpz_cmp(output_m->co[i], check->co[i]), 0);
    }

    mpz_clear(m_mod);
    freePlainText(a_m);
    freePlainText(b_m);
    freePlainText(output_m);
    freePlainText(check);
}

TEST(CRTTest, KeySwitch)
{
    Parameters* p = generateParameters(256, 2, 10);
    KeyPair* keys_1 = KeyGen(p);
    KeyPair* keys_2 = KeyGen(p);
    PlainText* a = newPlainText(p->n);
    PlainText* a_prime = newPlainText(p->n);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < a->order; i++)
    {
        mpz_urandomm(a->co[i], p->state, m_mod);
    }
    mpz_clear(m_mod);

    DblCRTCipherText c = CRTEnc(a, keys_1->pk, p);
    DblCRT hint = CRTKeySwitchHint(1, keys_1->sk, keys_2->sk, p);
    CRTKeySwitch(&c, c, hint, p);
    CRTDec(a_prime, c, keys_2->sk, p);

    for(uint64_t i = 0; i < a->order; i++)
    {
        ASSERT_EQ(mpz_cmp(a->co[i], a_prime->co[i]), 0);
    }

    freePlainText(a);
    freePlainText(a_prime);
}

TEST(CRTTest, ModReduction)
{
    Parameters* p = generateParameters(64, 2, 3);
    KeyPair* keys = KeyGen(p);

    PlainText *m = newPlainText(p->n);
    PlainText *m_prime = newPlainText(p->n);
    
    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, p->p);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(m->co[i], p->state, m_mod);
    }

    DblCRTCipherText c = CRTEnc(m, keys->pk, p);
    DblCRTCipherText c_reduced = CRTEnc(m, keys->pk, p);
    CRTModReduction(&c_reduced, c, p->q_factors[0], p->inv_q_factors[0], p);
    mpz_fdiv_q(m_mod, p->q, p->q_factors[0]);
    ASSERT_EQ(mpz_cmp(c_reduced.mod, m_mod), 0);
    CRTDec(m_prime, c_reduced, keys->sk, p);


    for(uint64_t i = 0; i < p->n; i++)
    {
        ASSERT_EQ(c_reduced.poly.values[0][i], 0);
    }
    for(uint64_t i = 0; i < p->n; i++)
    {
        ASSERT_EQ(mpz_cmp(m_prime->co[i], m->co[i]), 0);
    }

    freePlainText(m);
    freePlainText(m_prime);
    freeParameters(p);
    mpz_clear(m_mod);
}

TEST(CRTTest, ModReductionTwice)
{
    Parameters* p = generateParameters(64, 2, 3);
    KeyPair* keys = KeyGen(p);

    PlainText *m = newPlainText(p->n);
    PlainText *m_prime = newPlainText(p->n);
    
    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, p->p);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(m->co[i], p->state, m_mod);
    }

    DblCRTCipherText c = CRTEnc(m, keys->pk, p);
    DblCRTCipherText c_reduced = CRTEnc(m, keys->pk, p);
    CRTModReduction(&c_reduced, c, p->q_factors[0], p->inv_q_factors[0], p);
    CRTModReduction(&c_reduced, c_reduced, p->q_factors[1], p->inv_q_factors[1], p);
    CRTDec(m_prime, c_reduced, keys->sk, p);

    for(uint64_t i = 0; i < p->n; i++)
    {
        ASSERT_EQ(mpz_cmp(m_prime->co[i], m->co[i]), 0);
    }

    freePlainText(m);
    freePlainText(m_prime);
    freeParameters(p);
    mpz_clear(m_mod);
}

TEST(CRTTest, ModMatch)
{
    Parameters* p = generateParameters(64, 2, 4);
    KeyPair* keys = KeyGen(p);

    PlainText *m = newPlainText(p->n);
    PlainText *check = newPlainText(p->n);
    PlainText *m_prime = newPlainText(p->n);
    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, p->p);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(check->co[i], p->state, m_mod);
    }
    mpz_clear(m_mod);

    DblCRTCipherText c = CRTEnc(check, keys->pk, p);
    DblCRTCipherText c_prime = CRTEnc(check, keys->pk, p);
    CRTModReduction(&c, c, p->q_factors[0], p->inv_q_factors[0], p);
    CRTModReduction(&c, c, p->q_factors[1], p->inv_q_factors[1], p);

    CRTModMatch(&c, &c_prime, p);

    ASSERT_EQ(mpz_cmp(c.mod, c_prime.mod), 0);

    CRTDec(m, c, keys->sk, p);
    CRTDec(m_prime, c_prime, keys->sk, p);

    for(uint64_t i = 0; i < p->n; i++)
    {
        ASSERT_EQ(mpz_cmp(check->co[i], m->co[i]), 0);
        ASSERT_EQ(mpz_cmp(check->co[i], m_prime->co[i]), 0);
    }
    freePlainText(m);
    freePlainText(m_prime);
    freeParameters(p);
}

TEST(CRTTest, CompEvalMult)
{
    Parameters* p = generateParameters(64, 2, 2);
    KeyPair* keys_a = KeyGen(p);
    KeyPair* keys_b = KeyGen(p);
    Polynomial joint_sk = { NULL, p->n };
    initPoly(&joint_sk);
    ringMul(joint_sk, keys_a->sk, keys_b->sk, p);

    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    PlainText* ab_m = newPlainText(p->n);
    PlainText* ref = newPlainText(p->n);
    
    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a_m->co[i], p->state, m_mod);
        mpz_urandomm(b_m->co[i], p->state, m_mod);
    }

    DblCRTCipherText a_c = CRTEnc(a_m, keys_a->pk, p);
    DblCRTCipherText b_c = CRTEnc(b_m, keys_a->pk, p);
    DblCRTCipherText out = CRTEnc(b_m, keys_a->pk, p);
    DblCRT hint = CRTKeySwitchHint(2, joint_sk, keys_a->sk, p);
    CRTCompEvalMul(&out, &a_c, &b_c, hint, p);
    CRTDec(ab_m, out, keys_a->sk, p);

    ringMulModOrder(*ref, *a_m, *b_m, m_mod, p->n);

    ASSERT_EQ(out.degree, 1);
    for(uint64_t i = 0; i < p->n; i++) ASSERT_EQ(mpz_cmp(ref->co[i], ab_m->co[i]), 0);

	freeParameters(p);
	freePlainText(a_m);
	freePlainText(b_m);
	freePlainText(ab_m);
	freePlainText(ref);
}
