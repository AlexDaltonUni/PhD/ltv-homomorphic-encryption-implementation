#include <gtest/gtest.h>
#include <gmp.h>

extern "C" {
#include "../libltv/gmp_impl.h"
}

TEST(GmpImplTest, KeyGen)
{
    Parameters* p = generateParameters(200, 2, 1);
    KeyPair* keys = KeyGen(p);
    Polynomial check = { NULL, p->n };
    
    initPoly(&check);

    // f = 1 mod p
    for(uint64_t i = 1; i < check.order; i++)
    {
        mpz_mod_ui(check.co[i], keys->sk.co[i], p->p);
        ASSERT_EQ(mpz_cmp_ui(check.co[i], 0), 0);
    }
    mpz_mod_ui(check.co[0], keys->sk.co[0], p->p);

    ASSERT_EQ(mpz_cmp_ui(check.co[0], 1), 0);

    // f is invertable
    ASSERT_TRUE(ringInv(check, keys->sk, p));
    freePoly(&check);
    freeParameters(p);
}

TEST(GmpImplTest, Correctness)
{
    Parameters* p = generateParameters(128, 2, 1);
    KeyPair* keys = KeyGen(p);

    PlainText* m = newPlainText(p->n);
    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < m->order; i++)
    {
        mpz_urandomm(m->co[i], p->state, m_mod);
    }

    PlainText* m_prime = newPlainText(p->n);
    CipherText* c = newCipherText(p->n, p->q);

    Enc(c, m, keys->pk, p);
    Dec(m_prime, c, keys->sk, p);

    for(uint64_t i = 0; i < m->order; i++)
    {
        ASSERT_EQ(mpz_cmp(m->co[i], m_prime->co[i]), 0);
    }

    freePlainText(m);
    freePlainText(m_prime);
    freeCipherText(c);
    mpz_clear(m_mod);
    freeParameters(p);
}

TEST(GmpImplTest, EvalAddOneZeroAfterModReduction)
{
    Parameters* p = generateParameters(8, 2, 2);
    KeyPair* keys = KeyGen(p);

    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    PlainText* out = newPlainText(p->n);

    CipherText* a_c = newCipherText(p->n, p->q);
    CipherText* b_c = newCipherText(p->n, p->q);
    CipherText* check = newCipherText(p->n, p->q);

    setPoly(*a_m, 0);
    setPoly(*b_m, 0);
    mpz_set_ui(b_m->co[0], 1);

    Enc(a_c, a_m, keys->pk, p);
    Enc(b_c, b_m, keys->pk, p);

    ModReduction(a_c, a_c, p->q_factors[0], p->inv_q_factors[0], p);

    EvalAdd(check, a_c, b_c, p);

    Dec(out, check, keys->sk, p);

    printf(" output : "); printPoly(*out);

    for(uint64_t i = 1; i < p->n; i++)
    {
        ASSERT_EQ(mpz_cmp_ui(out->co[i], 0), 0);
    }

    ASSERT_EQ(mpz_cmp_ui(out->co[0], 1), 0);

    freePlainText(a_m);
    freePlainText(b_m);
    freePlainText(out);
    freeCipherText(check);
    freeCipherText(a_c);
    freeCipherText(b_c);
    freeParameters(p);
}

TEST(GmpImplTest, EvalAdd)
{
    Parameters* p = generateParameters(128, 2, 1);
    KeyPair* keys = KeyGen(p);

    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    PlainText* out = newPlainText(p->n);

    CipherText* a_c = newCipherText(p->n, p->q);
    CipherText* b_c = newCipherText(p->n, p->q);
    CipherText* check = newCipherText(p->n, p->q);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a_m->co[i], p->state, m_mod);
        mpz_urandomm(b_m->co[i], p->state, m_mod);
    }

    Enc(a_c, a_m, keys->pk, p);
    Enc(b_c, b_m, keys->pk, p);
    EvalAdd(check, a_c, b_c, p);
    Dec(out, check, keys->sk, p);

    polyAdd(check->poly, *a_m, *b_m);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_mod(check->poly.co[i], check->poly.co[i], m_mod);
        ASSERT_EQ(mpz_cmp(check->poly.co[i], out->co[i]), 0);
    }
    
    freePlainText(a_m);
    freePlainText(b_m);
    freePlainText(out);
    freeCipherText(check);
    freeCipherText(a_c);
    freeCipherText(b_c);
    mpz_clear(m_mod);
    freeParameters(p);
}

TEST(GmpImplTest, EvalSub)
{
    Parameters* p = generateParameters(128, 2, 1);
    KeyPair* keys = KeyGen(p);

    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    PlainText* out = newPlainText(p->n);

    CipherText* a_c = newCipherText(p->n, p->q);
    CipherText* b_c = newCipherText(p->n, p->q);
    CipherText* check = newCipherText(p->n, p->q);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a_m->co[i], p->state, m_mod);
        mpz_urandomm(b_m->co[i], p->state, m_mod);
    }

    Enc(a_c, a_m, keys->pk, p);
    Enc(b_c, b_m, keys->pk, p);
    EvalSub(check, a_c, b_c, p);
    Dec(out, check, keys->sk, p);

    polySub(check->poly, *a_m, *b_m);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_mod(check->poly.co[i], check->poly.co[i], m_mod);
        ASSERT_EQ(mpz_cmp(check->poly.co[i], out->co[i]), 0);
    }

    freePlainText(a_m);
    freePlainText(b_m);
    freePlainText(out);
    freeCipherText(check);
    freeCipherText(a_c);
    freeCipherText(b_c);
    mpz_clear(m_mod);
    freeParameters(p);
}

TEST(GmpImplTest, EvalMul)
{
    Parameters* p = generateParameters(64, 2, 1);
    KeyPair* keys = KeyGen(p);

    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    PlainText* out = newPlainText(p->n);

    CipherText* a_c = newCipherText(p->n, p->q);
    CipherText* b_c = newCipherText(p->n, p->q);
    CipherText* check = newCipherText(p->n, p->q);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a_m->co[i], p->state, m_mod);
        mpz_urandomm(b_m->co[i], p->state, m_mod);
    }

    Enc(a_c, a_m, keys->pk, p);
    Enc(b_c, b_m, keys->pk, p);
    EvalMul(check, a_c, b_c, p);
    ringMul(keys->sk, keys->sk, keys->sk, p);
    Dec(out, check, keys->sk, p);
    ASSERT_EQ(check->degree, a_c->degree + b_c->degree);

    Polynomial ref = { NULL, p->n * 2 };
    initPoly(&ref);
    polyMul(ref, *a_m, *b_m);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_sub(ref.co[i], ref.co[i], ref.co[i + p->n]);
        mpz_mod(ref.co[i], ref.co[i], m_mod);
    }

    for(uint64_t i = 0; i < p->n; i++)
    {
        ASSERT_EQ(mpz_cmp(ref.co[i], out->co[i]), 0);
    }

    freePoly(&ref);
    freePlainText(a_m);
    freePlainText(b_m);
    freePlainText(out);
    freeCipherText(check);
    freeCipherText(a_c);
    freeCipherText(b_c);
    mpz_clear(m_mod);
    freeParameters(p);
}

TEST(GmpImplTest, KeySwitch)
{
    Parameters* p = generateParameters(128, 2, 1);
    KeyPair* keys_a = KeyGen(p);
    KeyPair* keys_b = KeyGen(p);

    PlainText *m = newPlainText(p->n);
    CipherText *c = newCipherText(p->n, p->q);
    PlainText *m_prime = newPlainText(p->n);
    
    Polynomial hint = { NULL, p->n };
    initPoly(&hint);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, p->p);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(m->co[i], p->state, m_mod);
    }
    mpz_clear(m_mod);

    Enc(c, m, keys_a->pk, p);
    KeySwitchHint(hint, c->degree, keys_a->sk, keys_b->sk, p);
    KeySwitch(c, c, hint, p);
    ASSERT_EQ(c->degree, 1);
    Dec(m_prime, c, keys_b->sk, p);

    for(uint64_t i = 0; i < p->n; i++)
    {
        ASSERT_EQ(mpz_cmp(m_prime->co[i], m->co[i]), 0);
    }

    freePlainText(m);
    freeCipherText(c);
    freePlainText(m_prime);
    freePoly(&hint);
    freeParameters(p);
}

TEST(GmpImplTest, ModReduction)
{
    Parameters* p = generateParameters(128, 2, 3);
    KeyPair* keys = KeyGen(p);

    PlainText *m = newPlainText(p->n);
    CipherText *c = newCipherText(p->n, p->q);
    CipherText *c_reduced = newCipherText(p->n, p->q);
    PlainText *m_prime = newPlainText(p->n);
    
    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, p->p);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(m->co[i], p->state, m_mod);
    }

    Enc(c, m, keys->pk, p);
    ModReduction(c_reduced, c, p->q_factors[0], p->inv_q_factors[0], p);
    Dec(m_prime, c_reduced, keys->sk, p);

    mpz_fdiv_q(m_mod, p->q, p->q_factors[0]);
    ASSERT_EQ(mpz_cmp(c_reduced->mod, m_mod), 0);
    for(uint64_t i = 0; i < p->n / 2; i++)
    {
        ASSERT_EQ(mpz_cmp(m_prime->co[i], m->co[i]), 0);
    }

    freePlainText(m);
    freeCipherText(c);
    freeCipherText(c_reduced);
    freePlainText(m_prime);
    freeParameters(p);
}

TEST(GmpImplTest, RingReduction)
{
    Parameters* p = generateParameters(64, 2, 1);
    KeyPair* keys = KeyGen(p);
    KeyPair* sparse_keys = KeyGen(p);
    Polynomial reduced_sk = { NULL, p->n / 2 };
    initPoly(&reduced_sk);

    for(uint64_t i = 0; i < p->n / 2; i++)
    {
        mpz_set_ui(sparse_keys->sk.co[2*i + 1], 0);
        mpz_set(reduced_sk.co[i], sparse_keys->sk.co[2*i]);
    }

    PlainText *m = newPlainText(p->n);
    CipherText *c = newCipherText(p->n, p->q);
    CipherText *c_reduced = newCipherText(p->n, p->q);
    PlainText *m_prime = newPlainText(p->n / 2);
    
    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, p->p);
    for(uint64_t i = 0; i < p->n; i++)
    {
        (i % 2 == 1) ? mpz_set_ui(m->co[i], 0) : mpz_urandomm(m->co[i], p->state, m_mod);
    }

    Polynomial hint = { NULL, p->n };
    initPoly(&hint);

    Enc(c, m, keys->pk, p);
    KeySwitchHint(hint, c->degree, keys->sk, sparse_keys->sk, p);
    RingReduction(c_reduced, c, hint, p);
    Dec(m_prime, c_reduced, reduced_sk, p);

    ASSERT_EQ(c_reduced->poly.order, p->n);
    for(uint64_t i = 0; i < p->n / 2; i++)
    {
        ASSERT_EQ(mpz_cmp(m_prime->co[i], m->co[i * 2]), 0);
    }

    freePlainText(m);
    freeCipherText(c);
    freeCipherText(c_reduced);
    freePlainText(m_prime);
    freePoly(&hint);
    freePoly(&reduced_sk);
    freeParameters(p);
}

TEST(GmpImplTest, CompEvalMul)
{
    Parameters* p = generateParameters(64, 2, 2);
    KeyPair* keys_a = KeyGen(p);
    KeyPair* keys_b = KeyGen(p);
    Polynomial joint_sk = { NULL, p->n };
    Polynomial key_hint = { NULL, p->n };
    initPoly(&joint_sk);
    initPoly(&key_hint);
    ringMul(joint_sk, keys_a->sk, keys_b->sk, p);

    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    PlainText* ab_m = newPlainText(p->n);
    PlainText* ref = newPlainText(p->n);

    CipherText* a_c = newCipherText(p->n, p->q);
    CipherText* b_c = newCipherText(p->n, p->q);
    CipherText* out = newCipherText(p->n, p->q);
    CipherText* check = newCipherText(p->n, p->q);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a_m->co[i], p->state, m_mod);
        mpz_urandomm(b_m->co[i], p->state, m_mod);
    }

    Enc(a_c, a_m, keys_a->pk, p);
    Enc(b_c, b_m, keys_b->pk, p);
    KeySwitchHint(key_hint, 2, joint_sk, keys_a->sk, p);
    CompEvalMul(out, a_c, b_c, key_hint, p);
    Dec(ab_m, out, keys_a->sk, p);

    EvalMul(check, a_c, b_c, p);
    Dec(ref, check, joint_sk, p);

    ASSERT_EQ(out->degree, 1);
    for(uint64_t i = 0; i < p->n; i++) ASSERT_EQ(mpz_cmp(ref->co[i], ab_m->co[i]), 0);

    freePlainText(a_m);
    freePlainText(b_m);
    freePlainText(ab_m);
    freePlainText(ref);
    freeCipherText(out);
    freeCipherText(a_c);
    freeCipherText(b_c);
    freeCipherText(check);
    freePoly(&joint_sk);
    freePoly(&key_hint);
    mpz_clear(m_mod);
    freeParameters(p);
}

TEST(GmpImplTest, ModMatch)
{
    Parameters* p = generateParameters(64, 2, 20);
    KeyPair* keys = KeyGen(p);

    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    PlainText* check = newPlainText(p->n);
    CipherText* a_c = newCipherText(p->n, p->q);
    CipherText* b_c = newCipherText(p->n, p->q);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a_m->co[i], p->state, m_mod);
        mpz_urandomm(b_m->co[i], p->state, m_mod);
    }
    mpz_clear(m_mod);

    Enc(a_c, a_m, keys->pk, p);
    Enc(b_c, b_m, keys->pk, p);


    for(int i = 0; i < 10; i++) ModReduction(a_c, a_c, p->q_factors[i], p->inv_q_factors[i], p);

    modMatch(a_c, b_c, p);

    ASSERT_EQ(mpz_cmp(a_c->mod, b_c->mod), 0);

    Dec(check, a_c, keys->sk, p);
    for(uint64_t i = 0; i < check->order; i++) ASSERT_EQ(mpz_cmp(check->co[i], a_m->co[i]), 0);

    Dec(check, b_c, keys->sk, p);
    for(uint64_t i = 0; i < check->order; i++) ASSERT_EQ(mpz_cmp(check->co[i], b_m->co[i]), 0);

    freePlainText(a_m);
    freePlainText(b_m);
    freePlainText(check);
    freeCipherText(a_c);
    freeCipherText(b_c);
    freeParameters(p);
}
