"""Graphing script"""

import os
import math
import sys
import matplotlib.pyplot as plt
import numpy as np
import model

#pylint: disable=too-few-public-methods
#pylint: disable=missing-docstring
#pylint: disable=C0103

class TimingData():
    """Gets and holds all timing information"""
    def __init__(self):
        self.ring_orders = []
        self.all_threads = []
        if os.path.isfile("program_time.txt"):
            self.program_time = np.loadtxt("program_time.txt").tolist()
        else:
            print("Run data.py")
            sys.exit()

def get_model_time(n: int, c: int):
    hpb_model = model.HomomorphicBloodPressure(n, c)
    return hpb_model.time() * 0.9


def main():
    """harvests the data and creates the graph"""
    timings = TimingData()

    for i in range(len(timings.program_time)):
        ring_order_power = math.floor(i / 12)
        num_threads = i % 12 + 1
        ring_order = 2**ring_order_power
        timings.ring_orders.append(ring_order)
        timings.all_threads.append(num_threads)

    list_threads = range(1, 13)
    list_orders = []
    for i in range(math.ceil(len(timings.program_time)/12)):
        list_orders.append(2 ** i)


    surface_x, surface_y = np.meshgrid(list_threads, list_orders)
    surface_zs = np.array([
        get_model_time(y, x) for x, y in zip(np.ravel(surface_x), np.ravel(surface_y))
        ])
    surface_z = surface_zs.reshape(surface_x.shape)

    axes = plt.axes(projection='3d')
    axes.set_ylabel("Ring Order")
    axes.set_zlabel("Execution Time (s)")
    axes.set_xlabel("Threads")
    axes.scatter(timings.all_threads, timings.ring_orders, timings.program_time,
                 label="Actual", color="red")
    axes.plot_surface(surface_x, surface_y, surface_z, lw=0, rstride=1, cstride=1,
                      label="Model", alpha=0.75)
    contour_levels = [0, 1000, 2000, 3000, 4000, 5000]
    axes.contour(surface_x, surface_y, surface_z, contour_levels, colors="black")
    plt.show()

if __name__ == "__main__":
    main()
